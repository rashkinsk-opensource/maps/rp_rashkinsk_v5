# RP_RASHKINSK_V5


## Настройка среды разработки
Смонтировать контент карты (папку с materials/models) и контент Counter Strike Source в файле `%GMODDIR%/garrysmod/cfg/mount.cfg` как в примере:
```
//
// Use this file to mount additional paths to the filesystem
// DO NOT add a slash to the end of the filename
//

"mountcfg"
{
	"cstrike"	"D:\SteamLibrary\steamapps\common\Counter-Strike Source\cstrike"
	"mapdata"	"C:\Users\foverzar\Documents\Projects\rp_rashkinsk_v5\content"
}
```

Запустить Hammer Editor из папки `%GMODDIR%/bin/hammer.exe` -- он смонтирует все ресурсы, указанные в `mount.cfg`

## Cборка карты

В Hammer Editor запустить File -> Run Map. Выбрать режим advanced/expert.
![alt text](doc/run-map.png "Окно сборки")

Выбрать конфигурацию `Default`

Добавить следующие флаги к каждому компилятору

VBSP ($bsp_exe): `-notjunc -allowdynamicpropsasstatic`

VVIS ($vis_exe): `-fast` (lazy ass dev)


## Построение отражений

Для работы отражений необходимо вшить информацию о них в bsp-файл.

Для этого после завершения сборки и успешного запуска в Garry's Mod нужно построить cubemaps. В консоли gmod выполнить следующие команды:

(некоторые команды будут застравлять гмод подвисать или перезапускаться -- это нормально)
```
mat_specular 0
mat_hdr_level 0
buildcubemaps
mat_hdr_level 2
buildcubemaps

mat_specular 1 # Для включения отражений и оценки результата
```

Garry's Mod сам запишет полученную информацию об отражениях в bsp-файл (в папке maps). Этот файл затем можно загружать на сервер.

## Настройка FastDL

Содержимое каталога `content` положить на web-сервер и в файле `garrysmod/cfg/server.cfg` указать флаг `sv_downloadurl`:

```
sv_downloadrul http://mywebhosting.tld/content
```

В каталог `garrysmod/maps` сложить файл *.res файл из репозитория -- это необходимо чтобы клиент понял какие ресурсы ему надо выгрузить с fastdl.