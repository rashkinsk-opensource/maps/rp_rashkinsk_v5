#!/bin/bash

MAP="rp_rashkinsk_v5"

OUTPUT="../$MAP.res"

cd "content"

printf "Resources\n{\n\n" > $OUTPUT

find . -type f | sed -e 's/\.\/\(.*\)/"\1"  "file"/' >> $OUTPUT

printf "\n}" >> $OUTPUT